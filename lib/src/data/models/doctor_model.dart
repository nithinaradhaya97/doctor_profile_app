import 'package:bhimadoctor/objectbox.g.dart';
import 'package:objectbox/objectbox.dart';

@Entity()
class DoctorModel {
  int id = 0;
  late String firstName;
  late String lastName;
  late String profilePic;
  late bool favorite;
  late String primaryContactNo;
  late String rating;
  late String emailAddress;
  late String qualification;
  late String description;
  late String specialization;
  late String languagesKnown;
  late String? gender;
  late String? bloodGroup;
  late String? height;
  late String? weight;
  late String? day;
  late String? month;
  late String? year;

  DoctorModel({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.profilePic,
    required this.favorite,
    required this.primaryContactNo,
    required this.rating,
    required this.emailAddress,
    required this.qualification,
    required this.description,
    required this.specialization,
    required this.languagesKnown,
    this.gender,
    this.bloodGroup,
    this.height,
    this.weight,
    this.day,
    this.month,
    this.year,
  });

  DoctorModel.fromJson(Map<String, dynamic> json) {
    // id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    profilePic = json['profile_pic'];
    favorite = json['favorite'];
    primaryContactNo = json['primary_contact_no'];
    rating = json['rating'];
    emailAddress = json['email_address'];
    qualification = json['qualification'];
    description = json['description'];
    specialization = json['specialization'];
    languagesKnown = json['languagesKnown'];
    gender = json['gender'];
    bloodGroup = json['bloodGroup'];
    height = json['height'];
    weight = json['weight'];
    day = json['day'];
    month = json['month'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    // data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['profile_pic'] = this.profilePic;
    data['favorite'] = this.favorite;
    data['primary_contact_no'] = this.primaryContactNo;
    data['rating'] = this.rating;
    data['email_address'] = this.emailAddress;
    data['qualification'] = this.qualification;
    data['description'] = this.description;
    data['specialization'] = this.specialization;
    data['languagesKnown'] = this.languagesKnown;
    data['gender'] = this.gender;
    data['bloodGroup'] = this.bloodGroup;
    data['height'] = this.height;
    data['weight'] = this.weight;
    data['day'] = this.day;
    data['month'] = this.month;
    data['year'] = this.year;

    return data;
  }
}
