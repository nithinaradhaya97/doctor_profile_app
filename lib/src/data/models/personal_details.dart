import 'package:flutter/cupertino.dart';

class PersonalDetails {
  const PersonalDetails({
    required this.key,
    this.value,
    this.icon,
    this.textEditingController,
  });
  final String key;
  final String? value;
  final IconData? icon;
  final TextEditingController? textEditingController;
}
