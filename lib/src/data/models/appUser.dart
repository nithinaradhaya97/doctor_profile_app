import 'package:flutter/cupertino.dart';

class AppUser {
  const AppUser(
      {required this.uid,
      this.email,
      this.photoUrl,
      this.displayName,
      this.role,
      required this.number});

  final String uid;
  final String? email;
  final String? photoUrl;
  final String? displayName;
  final int? role;
  final String number;
}
