abstract class BaseDBStoreHelper {
  Future<int> insertItem(dynamic item);
  Future<bool> deleteItem(int id);
  Future<List<int>> insertAll(
    List<dynamic> item,
  );
  Future<List<dynamic>> queryAllPersons();
  editItem(dynamic item);
  Future<int> deleteAllItems();
}
