import 'package:bhimadoctor/objectbox.g.dart';
import 'package:bhimadoctor/src/data/models/doctor_model.dart';
import 'package:objectbox/src/native/box.dart';

import 'base_db_store_helper.dart';

class DBStoreHelper implements BaseDBStoreHelper {
  late Box<DoctorModel> dbox;

  DBStoreHelper(Store store) {
    dbox = store.box<DoctorModel>();
  }

  @override
  Future<bool> deleteItem(int id) async {
    return await dbox.remove(id);
  }

  @override
  editItem(item) async {
    await dbox.remove(item.id);
    await dbox.put(item);
  }

  @override
  Future<List<int>> insertAll(List item) async {
    return await dbox.putMany(item as List<DoctorModel>);
  }

  @override
  Future<int> insertItem(dynamic item) async {
    return await dbox.put(item);
  }

  @override
  Future<List<DoctorModel>> queryAllPersons() async {
    return await dbox.getAll();
  }

  @override
  Future<int> deleteAllItems() async {
    return await dbox.removeAll();
  }
}
