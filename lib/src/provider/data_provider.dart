import 'dart:convert';

abstract class BaseModel {
  BaseModel.fromJson(Map<String, dynamic> json);
}

class ApiResponse<T extends BaseModel> {
  late int code;
  late T? data;

  ApiResponse(this.code, this.data);

  ApiResponse.create(int statusCode, String? json) {
    code = statusCode;
    if (json != null) {
      data = (T as dynamic).fromJson(jsonDecode(json));
    }
  }
}

class ListApiResponse<T> {
  late int code;
  late List<T>? contents;
  late int total;
  ListApiResponse(this.code, this.total, this.contents);
}

abstract class DataProvider {
  Future<ApiResponse<T>> get<T extends BaseModel>(Uri resources);

  // Future<ListApiResponse<T>> getList<T>(Uri resources, dynamic parser, {Pagination? pagination, Sort? sort});

  Future<ReferenceArrayResponse> getReferenceArray(Uri resources);

  Future<CreateResponse<T>> create<T>(Uri resources, dynamic body);

  Future<BaseResponse> deleteMany<T>(Uri resources, List<String> ids);

// Future<T> update<T>(Uri resources, dynamic body);
//
// Future<T> updateMany<T>(Uri resources, dynamic body);
//
// Future<T> delete<T>(Uri resources);
//
// Future<T> deleteMany<T>(Uri resources, List<String> ids);
//
// Future<T> getMany<T>(Uri resources);
//
// Future<T> getManyReference<T>(Uri resources, List<String> ids);
//
// Future<T> getOne<T>(Uri resources);
//
// Future<T> get<T>(Uri resources);
} // avoids adding a c

class BaseResponse<T> {
  final int statusCode;
  final String statusMessage;
  List<T>? data;

  BaseResponse(this.statusCode, this.statusMessage, {this.data});
}

class CreateResponse<T> {
  T data;

  CreateResponse(this.data);
}

class ReferenceArrayResponse {
  List<ReferenceItem> data;

  ReferenceArrayResponse(this.data);
}

class ReferenceItem {
  int id;
  String name;

  ReferenceItem(this.id, this.name);
}
