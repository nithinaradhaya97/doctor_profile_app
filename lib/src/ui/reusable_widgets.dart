import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  final String mtext;
  final TextStyle? mstyle;
  const CustomText({required this.mtext, this.mstyle});

  @override
  Widget build(BuildContext context) {
    return Text(mtext, style: mstyle);
  }
}

class PhoneNumberField extends StatelessWidget {
  final TextEditingController controller;
  final String prefix;

  const PhoneNumberField(
      {Key? key, required this.controller, required this.prefix})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          Text("  " + prefix + "  ", style: TextStyle(fontSize: 16.0)),
          SizedBox(width: 8.0),
          Expanded(
            child: TextFormField(
              controller: controller,
              autofocus: false,
              keyboardType: TextInputType.phone,
              key: const Key('EnterPhone-TextFormField'),
              decoration: const InputDecoration(
                border: InputBorder.none,
                errorMaxLines: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child: Center(child: CircularProgressIndicator()));
  }
}

class Error extends StatelessWidget {
  final String message;

  Error(this.message);

  @override
  Widget build(BuildContext context) {
    return Container(child: Center(child: Text("Error: $message")));
  }
}

class ErrorMessage extends StatelessWidget {
  late String errorMessage;
  ErrorMessage(String msg) {
    errorMessage = msg;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(child: Text("Error appeared." + errorMessage)));
  }
}

Widget? showSnackBar(String message, BuildContext context) {
  final snackBar = SnackBar(content: Text(message));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

class GetSizedBox15 extends StatelessWidget {
  const GetSizedBox15({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: 5.0);
  }
}
