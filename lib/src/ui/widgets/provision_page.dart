import 'package:bhimadoctor/src/constants/colors.dart';
import 'package:bhimadoctor/src/constants/font_family.dart';
import 'package:bhimadoctor/src/constants/screen_name_routes.dart';
import 'package:bhimadoctor/src/constants/strings.dart';
import 'package:bhimadoctor/src/ui/reusable_widgets.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/auth_provider.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/firebase_authentication_provider.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class ProvisionPage extends StatefulWidget {
  const ProvisionPage({Key? key}) : super(key: key);

  @override
  _ProvisionPageState createState() => _ProvisionPageState();
}

class _ProvisionPageState extends State<ProvisionPage> {
  String dialCode = '+91';

  final fieldText = TextEditingController();
  void clearText() {
    fieldText.clear();
  }

  bool _isLoaderVisible = false;
  FirebaseAuth auth = FirebaseAuth.instance;
  String? fbIdToken;
  bool _isCodeSent = false;
  _onVerificationCompleted(PhoneAuthCredential authCredential) async {
    User? user = FirebaseAuth.instance.currentUser;

    if (authCredential.smsCode != null) {
      try {
        UserCredential credential =
            await user!.linkWithCredential(authCredential);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'provider-already-linked') {
          await auth.signInWithCredential(authCredential);
        }
      }
    }
  }

  _onVerificationFailed(FirebaseAuthException exception) {
    setState(() {
      _isLoaderVisible = false;
    });
    if (exception.code == 'invalid-phone-number' && !_isCodeSent) {
      showSnackBar("The phone number entered is invalid!", context);
    }
  }

  _onCodeSent(String verificationId, int? forceResendingToken) {
    setState(() {
      _isLoaderVisible = false;
      _isCodeSent = true;
    });
    Navigator.of(context).pushNamed(ScreenNamesRoutes.VERIFYOTP_PAGE);
  }

  _onCodeTimeout(String timeout) {
    if (!_isCodeSent) showSnackBar("Timeout.Please Try Again", context);
    return null;
  }

  _startPhoneAuth(
    String number,
  ) async {
    setState(() {
      _isLoaderVisible = true;
    });
    await Future.delayed(const Duration(seconds: 5));
    final phoneAuthDataProvider =
        Provider.of<FirebaseAuthenticationProvider>(context, listen: false);
    await phoneAuthDataProvider.verifyPhoneNumber(
        number,
        _onVerificationCompleted,
        _onVerificationFailed,
        _onCodeSent,
        _onCodeTimeout);
  }

  void _onCountryChange(CountryCode countryCode) {
    setState(() {
      dialCode = countryCode.dialCode.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        color: CustomColors.kPrimaryDark,
        child: Column(
          children: [
            Expanded(
              flex: 50,
              child: Container(
                margin: EdgeInsets.only(top: 100),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CustomText(
                        mtext: Strings.kEnterNumber,
                        mstyle: const TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w600,
                            color: CustomColors.kWhiteColor,
                            fontFamily: FontFamily.robotoCondensedBold),
                      ),
                      Container(
                          margin: EdgeInsets.all(20.0),
                          child: Column(children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                      mtext: dialCode,
                                      mstyle: const TextStyle(
                                          fontFamily:
                                              FontFamily.robotoCondensedRegular,
                                          fontSize: 28,
                                          color: CustomColors.kGreenColor)),
                                  CountryCodePicker(
                                    onChanged: _onCountryChange,
                                    hideMainText: true,
                                    showFlagMain: false,
                                    showFlag: true,
                                    initialSelection: 'TF',
                                    hideSearch: false,
                                    showCountryOnly: false,
                                    showDropDownButton: true,
                                    showOnlyCountryWhenClosed: true,
                                    alignLeft: true,
                                  ),
                                  Expanded(
                                    child: TextField(
                                      maxLength: 10,
                                      keyboardType: TextInputType.number,
                                      inputFormatters: <TextInputFormatter>[
                                        FilteringTextInputFormatter.digitsOnly
                                      ], //
                                      controller: fieldText,
                                      style: const TextStyle(
                                          fontFamily:
                                              FontFamily.robotoCondensedRegular,
                                          fontSize: 28.0,
                                          color: CustomColors.kColorAccent),
                                      decoration: InputDecoration(
                                        counterText: '',
                                        enabledBorder:
                                            const UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: CustomColors.kPrimaryDark),
                                        ),
                                        focusedBorder:
                                            const UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: CustomColors.kPrimaryDark),
                                        ),
                                        suffixIcon: IconButton(
                                          // Icon to
                                          onPressed: clearText,
                                          icon: const Icon(
                                            Icons.close,
                                            color: CustomColors.kColorAccent,
                                          ), // clear text
                                        ),
                                        labelStyle: const TextStyle(
                                            color: CustomColors.kWhiteColor),
                                      ),
                                    ),
                                  )
                                ]),
                            const Divider(
                              thickness: 2,
                              color: Colors.grey,
                            ),
                          ])),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: CustomText(
                          mtext: Strings.KSmsSubtitle,
                          mstyle: const TextStyle(
                              height: 1.5,
                              fontSize: 18,
                              color: CustomColors.kWhiteColor,
                              fontFamily: FontFamily.robotoCondensedBold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 30),
            Visibility(
              visible: this._isLoaderVisible,
              child: Center(
                  child: CircularProgressIndicator(
                color: CustomColors.kWhiteColor,
              )),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 36.0, vertical: 60),
              child: SizedBox(
                width: double.infinity,
                child: TextButton(
                    onPressed: () {
                      if (fieldText.text.length == 10) {
                        _startPhoneAuth('$dialCode${fieldText.text}');
                      } else {
                        showSnackBar(
                            'Please Enter 10 digit phone number', context);
                      }
                    },
                    child: CustomText(
                        mtext: Strings.kContinueString,
                        mstyle: const TextStyle(
                          color: CustomColors.kWhiteColor,
                          fontSize: 25,
                        )),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(CustomColors.kGreenColor),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        )))),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
