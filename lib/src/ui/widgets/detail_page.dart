import 'package:bhimadoctor/src/constants/colors.dart';
import 'package:bhimadoctor/src/constants/font_family.dart';
import 'package:bhimadoctor/src/data/models/doctor_model.dart';
import 'package:bhimadoctor/src/data/models/personal_details.dart';
import 'package:bhimadoctor/src/network/client_Api.dart';
import 'package:bhimadoctor/src/ui/reusable_widgets.dart';
import 'package:bhimadoctor/src/ui/widgets/new.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/firebase_authentication_provider.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/firebase_authentication_provider.dart';
import 'package:bhimadoctor/src/utils/util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

bool isEditMode = false;

class _DetailPageState extends State<DetailPage> {
  List<PersonalDetails> _mainList = [];
  List<PersonalDetails> _getGridDetailsMap = [];

  @override
  Widget build(BuildContext context) {
    final clientApi = Provider.of<ClientApi>(context, listen: false);
    final firebaseAuthenticationProvider =
        Provider.of<FirebaseAuthenticationProvider>(context, listen: false);
    _mainList = [
      PersonalDetails(
          value: clientApi.selectedItem.firstName,
          key: 'FIRST NAME',
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.lastName,
          key: 'LAST NAME',
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.gender,
          key: 'GENDER',
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.primaryContactNo,
          key: 'CONTACT NUMBER',
          textEditingController: TextEditingController()),
    ];

    _getGridDetailsMap = [
      PersonalDetails(
          value: clientApi.selectedItem.day,
          key: 'DAY',
          icon: Icons.calendar_today,
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.month,
          key: 'MONTH',
          icon: Icons.calendar_today,
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.year,
          key: 'YEAR',
          icon: Icons.calendar_today,
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.bloodGroup,
          key: 'BLOOD GROUP',
          icon: Icons.bloodtype,
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.height,
          key: 'HEIGHT',
          icon: Icons.height,
          textEditingController: TextEditingController()),
      PersonalDetails(
          value: clientApi.selectedItem.weight,
          key: 'WEIGHT',
          icon: Icons.monitor_weight,
          textEditingController: TextEditingController()),
    ];

    return Consumer<ClientApi>(builder: (context, clientApiObject, child) {
      return SafeArea(
        child: Scaffold(
          body: Container(
            color: CustomColors.kPrimaryDark,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      size: 30,
                      color: CustomColors.kColorAccent,
                    ),
                  ),
                ),
                Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ProfileDetailWidget(
                          // mIndex: clientApi.selectedItem,
                          firebaseAuthenticationProvider:
                              firebaseAuthenticationProvider,
                          lisTileList: _mainList,
                          gridList: _getGridDetailsMap,
                          clientApi: clientApi,
                          doctorModel: clientApi.selectedItem),
                      margin: EdgeInsets.only(top: 80),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25.0),
                            topLeft: Radius.circular(25.0)),
                        gradient: LinearGradient(
                          colors: [
                            CustomColors.kWhiteColor,
                            CustomColors.kWhiteColor,
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          stops: [0, 0],
                        ),
                      ),
                    ),
                    Container(
                      child: Positioned(
                        top: 10,
                        height: 120,
                        child: CachedNetworkImage(
                            imageUrl: clientApi.selectedItem.profilePic,
                            imageBuilder: (context, imageProvider) => Container(
                                  width: 90.0,
                                  height: 100.0,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.white),
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: imageProvider, fit: BoxFit.fill),
                                  ),
                                ),
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) => const Image(
                                  width: 100,
                                  image: AssetImage(
                                      'assets/images/profile_placeholder.png'),
                                )),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class ListItem extends StatefulWidget {
  final int? index;
  final List<PersonalDetails>? lisTileList;
  ListItem({Key? key, this.index, this.lisTileList}) : super(key: key);

  @override
  _ListItemState createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  @override
  void initState() {
    super.initState();
    widget.lisTileList![widget.index!].textEditingController!.text =
        widget.lisTileList![widget.index!].value ?? '  --  ';
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: isEditMode,
      style: TextStyle(fontFamily: FontFamily.robotoCondensedRegular),
      controller: widget.lisTileList![widget.index!].textEditingController,
    );
  }
}

class GridListItem extends StatefulWidget {
  final int? index;
  final List<PersonalDetails>? list;
  GridListItem({Key? key, this.index, required this.list}) : super(key: key);
  @override
  _GridListItemState createState() => _GridListItemState();
}

class _GridListItemState extends State<GridListItem> {
  @override
  void initState() {
    super.initState();
    widget.list![widget.index!].textEditingController!.text =
        widget.list![widget.index!].value ?? '  -- ';
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: isEditMode,
      style: TextStyle(fontFamily: FontFamily.robotoCondensedRegular),
      textAlign: TextAlign.center,
      controller: widget.list![widget.index!].textEditingController,
    );
  }
}

class ProfileDetailWidget extends StatefulWidget {
  // final int mIndex;
  final FirebaseAuthenticationProvider firebaseAuthenticationProvider;
  final List<PersonalDetails> lisTileList;
  final List<PersonalDetails> gridList;
  final DoctorModel doctorModel;
  final ClientApi clientApi;
  const ProfileDetailWidget(
      {Key? key,
      required this.firebaseAuthenticationProvider,
      // required this.mIndex,
      required this.lisTileList,
      required this.gridList,
      required this.doctorModel,
      required this.clientApi})
      : super(key: key);

  @override
  _ProfileDetailWidgetState createState() => _ProfileDetailWidgetState();
}

class _ProfileDetailWidgetState extends State<ProfileDetailWidget> {
  @override
  Widget build(BuildContext context) {
    // final index = widget.mIndex;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 40),
          child: CustomText(
            mtext: widget.doctorModel.firstName +
                ' ' +
                widget.doctorModel.lastName,
            // '${widget.lisTileList[0].textEditingController?.text} ${widget.lisTileList[1].textEditingController?.text}',
            mstyle: const TextStyle(
                fontSize: 20, fontFamily: FontFamily.robotoCondensedRegular),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: SizedBox(
            width: 130,
            height: 35,
            child: TextButton(
                onPressed: () {
                  setState(() {
                    //   if (isAdmin(widget.firebaseAuthenticationProvider.mPhoneNumber)) {
                    if (!isEditMode) {
                      isEditMode = true;
                    } else {
                      final doctorModel = widget.doctorModel;
                      final gridList = widget.gridList;
                      final lisTileList = widget.lisTileList;
                      doctorModel.day = gridList[0].textEditingController?.text;
                      doctorModel.month =
                          gridList[1].textEditingController?.text;
                      doctorModel.year =
                          gridList[2].textEditingController?.text;
                      doctorModel.bloodGroup =
                          gridList[3].textEditingController?.text;
                      doctorModel.height =
                          gridList[4].textEditingController?.text;
                      doctorModel.weight =
                          gridList[5].textEditingController?.text;
                      doctorModel.firstName =
                          lisTileList[0].textEditingController?.text ?? '';
                      doctorModel.lastName =
                          lisTileList[1].textEditingController?.text ?? '';
                      doctorModel.gender =
                          lisTileList[2].textEditingController?.text ?? '';
                      doctorModel.primaryContactNo =
                          lisTileList[3].textEditingController?.text ?? '';

                      widget.clientApi.update(widget.clientApi.selectedItem,
                          widget.clientApi.selectedIndex);
                      isEditMode = false;
                      Navigator.of(context).pop();
                    }
                    //   }
                  });
                },
                child: CustomText(
                    mtext: !isEditMode ? 'EDIT PROFILE' : 'SAVE PROFILE',
                    mstyle: const TextStyle(
                      color: CustomColors.kWhiteColor,
                    )),
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(CustomColors.kGreenColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    )))),
          ),
        ),
        Expanded(
            child: Padding(
                padding: const EdgeInsets.only(top: 5.0, left: 8, right: 8),
                child: Container(
                  color: Color(0xFFf7f9fa),
                  child: SingleChildScrollView(
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: CustomText(
                            mtext: 'PERSONAL DETAILS',
                            mstyle: TextStyle(
                                fontFamily: FontFamily.robotoCondensedRegular,
                                fontSize: 20),
                          ),
                        ),
                      ),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.lisTileList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: const EdgeInsets.only(top: 5.0, left: 5),
                              color: CustomColors.kWhiteColor,
                              child: ListTile(
                                title: CustomText(
                                  mtext: widget.lisTileList[index].key,
                                  mstyle: TextStyle(
                                    color: CustomColors.kGreyColor,
                                    fontSize: 15,
                                  ),
                                ),
                                subtitle: ListItem(
                                  lisTileList: widget.lisTileList,
                                  index: index,
                                ),
                              ),
                            );
                          }),
                      SingleChildScrollView(
                        physics: ScrollPhysics(),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                              height: 200,
                              child: GridView.builder(
                                itemCount: widget.gridList.length,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        childAspectRatio: 5 / 4,
                                        crossAxisCount: 3,
                                        crossAxisSpacing: 1.0,
                                        mainAxisSpacing: 1.0),
                                itemBuilder: (BuildContext context, int index) {
                                  return SizedBox(
                                    height: 10,
                                    child: Card(
                                      color: CustomColors.kWhiteColor,
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  widget.gridList[index].icon,
                                                  size: 20.0,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(5.0),
                                                  child: Text(
                                                    widget.gridList[index].key,
                                                    style: TextStyle(
                                                      color: CustomColors
                                                          .kGreyColor,
                                                      fontSize: 10,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            GridListItem(
                                              index: index,
                                              list: widget.gridList,
                                            )
                                          ]),
                                    ),
                                  );
                                },
                              )),
                        ),
                      )
                    ]),
                  ),
                )))
      ],
    );
  }
}
