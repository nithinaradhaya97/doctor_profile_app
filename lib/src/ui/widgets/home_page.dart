import 'package:bhimadoctor/src/constants/colors.dart';
import 'package:bhimadoctor/src/constants/font_family.dart';
import 'package:bhimadoctor/src/constants/screen_name_routes.dart';
import 'package:bhimadoctor/src/data/models/doctor_model.dart';
import 'package:bhimadoctor/src/network/client_Api.dart';
import 'package:bhimadoctor/src/ui/reusable_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../objectbox.g.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

// DoctorModel? model;

class _HomePageState extends State<HomePage> {
  bool isGridView = false;
  @override
  Widget build(BuildContext context) {
    // final clientApi = Provider.of<ClientApi>(context, listen: false);
    final store = Provider.of<Store>(context, listen: false);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: CustomColors.kPrimaryDark,
          onPressed: () {
            setState(() {
              isGridView = !isGridView;
            });
          },
          child: Icon(
            isGridView ? Icons.menu : Icons.window,
          ),
        ),
        appBar: AppBar(
            backgroundColor: CustomColors.kWhiteColor,
            leading: Icon(
              Icons.menu,
              color: Colors.black,
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                    child: Image(
                        image: AssetImage('assets/images/logo_title.png'))),
                Expanded(
                    child: Image(image: AssetImage('assets/images/logo.png'))),
              ],
            )),
        body: Padding(
            padding: EdgeInsets.only(top: 8),
            child:
                Consumer<ClientApi>(builder: (context, clientApiObject, child) {
              return FutureBuilder<dynamic>(
                  future: clientApiObject.getDoctorList(store),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<DoctorModel> data = snapshot.data;

                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CarouselSlider(
                            options: CarouselOptions(
                              height: 130.0,
                              autoPlay: true,
                              autoPlayInterval: Duration(seconds: 3),
                              autoPlayAnimationDuration:
                                  Duration(milliseconds: 800),
                              autoPlayCurve: Curves.fastOutSlowIn,
                            ),
                            items: data.map((i) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return Card(
                                    color: CustomColors.kPrimaryDark,
                                    elevation: 5,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    )),
                                    child: ListTile(
                                        leading: ImageLoader(
                                            url: i.profilePic,
                                            isCarousel: true),
                                        title: Padding(
                                          padding: const EdgeInsets.all(6.0),
                                          child: Text(
                                            '${i.description}',
                                            maxLines: 6,
                                            style: TextStyle(
                                                color: CustomColors.kWhiteColor,
                                                fontSize: 12.0),
                                          ),
                                        ),
                                        subtitle: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 4.0),
                                          child: Text(
                                            ' - ${i.firstName} ${i.lastName}',
                                            textAlign: TextAlign.end,
                                            style: TextStyle(
                                                color: CustomColors.kWhiteColor,
                                                fontSize: 10.0),
                                          ),
                                        )),
                                  );
                                },
                              );
                            }).toList(),
                          ),

                          // Container(margin: const EdgeInsets.only(top: 10.0)),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: Align(
                                child: GridView.builder(
                                    itemCount: data.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      childAspectRatio: isGridView
                                          ? width / (height / 3)
                                          : width / (height / 8),
                                      //
                                      // childAspectRatio:
                                      //     (MediaQuery.of(context)
                                      //             .size
                                      //             .height) /
                                      //         MediaQuery.of(context)
                                      //             .size
                                      //             .width,

                                      crossAxisCount: isGridView ? 2 : 1,
                                    ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                  // border: Border.all(
                                                  //     color: Colors.black,
                                                  //     width: 0.1)
                                                  ),
                                              child: ListTile(
                                                onTap: () {
                                                  clientApiObject.selectedItem =
                                                      data[index];
                                                  clientApiObject
                                                      .selectedIndex = index;
                                                  Navigator.of(context)
                                                      .pushNamed(
                                                          ScreenNamesRoutes
                                                              .DETAIL_PAGE);
                                                },
                                                isThreeLine: true,
                                                leading: ImageLoader(
                                                  url: data[index].profilePic,
                                                  isCarousel: false,
                                                ),
                                                trailing: !isGridView
                                                    ? Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons
                                                                .arrow_forward_ios_outlined,
                                                            color: CustomColors
                                                                .kBlueDarkColor,
                                                          ),
                                                        ],
                                                      )
                                                    : null,
                                                title: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      data[index].firstName +
                                                          ' ' +
                                                          data[index].lastName,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                          fontFamily: FontFamily
                                                              .robotoCondensedRegular,
                                                          color: CustomColors
                                                              .kColorPrimary),
                                                    ),
                                                    Text(
                                                      data[index]
                                                          .specialization,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color: CustomColors
                                                              .kColorPrimary),
                                                    ),
                                                  ],
                                                ),
                                                subtitle: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 4.0),
                                                  child: Text(
                                                    data[index].description,
                                                    maxLines: 2,
                                                    // style: TextStyle(
                                                    //   fontSize: 15,
                                                    // ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            // const Expanded(
                                            //   child: Divider(
                                            //     thickness: 1,
                                            //   ),
                                            // )
                                          ],
                                        ),
                                      );
                                    }),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return const Center(child: CircularProgressIndicator());
                  });
            })));
  }
}

class ImageLoader extends StatelessWidget {
  final String url;
  final bool isCarousel;
  const ImageLoader({Key? key, required this.url, required this.isCarousel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      child: CachedNetworkImage(
          imageUrl: url,
          imageBuilder: (context, imageProvider) => Container(
                // width: 80.0,
                // height: 80.0,
                decoration: BoxDecoration(
                  shape: !isCarousel ? BoxShape.circle : BoxShape.rectangle,
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.fill),
                ),
              ),
          placeholder: (context, url) => CircularProgressIndicator(
                color: CustomColors.kWhiteColor,
              ),
          errorWidget: (context, url, error) => const Image(
                image: AssetImage('assets/images/profile_placeholder.png'),
              )),
    );
  }
}
