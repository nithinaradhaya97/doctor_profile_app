import 'package:bhimadoctor/src/constants/colors.dart';
import 'package:bhimadoctor/src/constants/font_family.dart';
import 'package:bhimadoctor/src/constants/screen_name_routes.dart';
import 'package:bhimadoctor/src/constants/strings.dart';
import 'package:bhimadoctor/src/ui/reusable_widgets.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/firebase_authentication_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class VerifyOtpPage extends StatefulWidget {
  const VerifyOtpPage({Key? key}) : super(key: key);

  @override
  _VerifyOtpPageState createState() => _VerifyOtpPageState();
}

class _VerifyOtpPageState extends State<VerifyOtpPage> {
  bool _isChecked = false;
  _toggleChecked() {
    setState(() {
      _isChecked = true;
    });
  }

  FocusNode focusNode1 = FocusNode();
  FocusNode focusNode2 = FocusNode();
  FocusNode focusNode3 = FocusNode();
  FocusNode focusNode4 = FocusNode();
  FocusNode focusNode5 = FocusNode();
  FocusNode focusNode6 = FocusNode();
  String code = "";

  Widget getPinField({required String key, required FocusNode focusNode}) =>
      SizedBox(
        height: 43.0,
        width: 40.0,
        child: Container(
          color: CustomColors.kBlueDarkColor,
          child: TextField(
            decoration: const InputDecoration(
              counterText: "",
            ),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            maxLength: 1,
            key: Key(key),
            expands: false,
//          autofocus: key.contains("1") ? true : false,
            autofocus: false,
            focusNode: focusNode,
            onChanged: (String value) {
              if (value.length == 1) {
                code += value;
                switch (code.length) {
                  case 1:
                    FocusScope.of(context).requestFocus(focusNode2);
                    break;
                  case 2:
                    FocusScope.of(context).requestFocus(focusNode3);
                    break;
                  case 3:
                    FocusScope.of(context).requestFocus(focusNode4);
                    break;
                  case 4:
                    FocusScope.of(context).requestFocus(focusNode5);
                    break;
                  case 5:
                    FocusScope.of(context).requestFocus(focusNode6);
                    break;
                  default:
                    FocusScope.of(context).requestFocus(FocusNode());
                    break;
                }
              }
            },
            maxLengthEnforced: true,
            textAlign: TextAlign.center,
            cursorColor: CustomColors.kColorAccent,
            style: const TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.w600,
                color: CustomColors.kColorAccent),
          ),
        ),
      );
  _OTPVerified(String data) {
    setState(() {
      this._isLoaderVisible = false;
    });
    if (data == 'Success') {
      showSnackBar('Successfully verified', context);
      Navigator.of(context).pushReplacementNamed(ScreenNamesRoutes.HOME_PAGE);
    } else {
      showSnackBar('Invalid OTP Please Try again', context);
    }
  }

  late FirebaseAuthenticationProvider phoneAuthDataProvider;
  bool _isLoaderVisible = false;

  @override
  Widget build(BuildContext context) {
    phoneAuthDataProvider =
        Provider.of<FirebaseAuthenticationProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
          child: Container(
        color: CustomColors.kPrimaryDark,
        child: Column(
          children: [
            Expanded(
              flex: 70,
              child: Container(
                margin: const EdgeInsets.only(top: 100),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: CustomText(
                          mtext: Strings.kEnterCode,
                          mstyle: const TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w600,
                              color: CustomColors.kWhiteColor,
                              fontFamily: FontFamily.robotoCondensedBold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 25, horizontal: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            getPinField(key: "1", focusNode: focusNode1),
                            const GetSizedBox15(),
                            getPinField(key: "2", focusNode: focusNode2),
                            const GetSizedBox15(),
                            getPinField(key: "3", focusNode: focusNode3),
                            const GetSizedBox15(),
                            getPinField(key: "4", focusNode: focusNode4),
                            const GetSizedBox15(),
                            getPinField(key: "5", focusNode: focusNode5),
                            const GetSizedBox15(),
                            getPinField(key: "6", focusNode: focusNode6),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: CustomText(
                          mtext:
                              '${Strings.kverifyOtpSubtitle} ${phoneAuthDataProvider != null ? '${phoneAuthDataProvider.mPhoneNumber}' : ''}',
                          mstyle: const TextStyle(
                              fontSize: 17,
                              height: 1.3,
                              color: CustomColors.kWhiteColor,
                              fontFamily: FontFamily.robotoCondensedBold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 30),
            Visibility(
              visible: this._isLoaderVisible,
              child: Center(
                  child: CircularProgressIndicator(
                color: CustomColors.kWhiteColor,
              )),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  right: 36.0, left: 36.0, bottom: 5, top: 45),
              child: SizedBox(
                height: 45,
                width: double.infinity,
                child: TextButton(
                    onPressed: () {
                      if (_isChecked) {
                        setState(() {
                          this._isLoaderVisible = true;
                        });
                        phoneAuthDataProvider.verifyAuthOtp(
                            smsCode: '${code}', mCallBackSuccess: _OTPVerified);
                      } else {
                        showSnackBar(
                            'Please agree the terms & policy', context);
                      }
                      // _startPhoneAuth('+919448160564', newContext);
                      // phoneSignIn(phoneNumber: '$dialCode${fieldText.text}');
                      // AuthProvider()
                    },
                    child: CustomText(
                        mtext: Strings.kLoginString,
                        mstyle: const TextStyle(
                          color: CustomColors.kWhiteColor,
                          fontSize: 25,
                        )),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(CustomColors.kGreenColor),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Checkbox(
                      value: _isChecked,
                      onChanged: (bool? value) {
                        setState(() {
                          _isChecked = value!;
                        });
                      }),
                  RichText(
                    text: TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'I agree to the ',
                            style: TextStyle(
                                fontSize: 15, color: CustomColors.kWhiteColor)),
                        TextSpan(
                            text: 'Terms Of Use',
                            style: new TextStyle(
                                fontSize: 15,
                                color: CustomColors.kColorAccent)),
                        TextSpan(
                            text: ' and ',
                            style: TextStyle(
                                fontSize: 15, color: CustomColors.kWhiteColor)),
                        TextSpan(
                            text: ' Privacy Policy ',
                            style: new TextStyle(
                                fontSize: 15,
                                color: CustomColors.kColorAccent)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }
}
