import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomColors {
  CustomColors._();

  static const kColorPrimary = Color(0xFF015ecb);
  static const kPrimaryDark = Color(0xFF2F579F);
  static const kColorAccent = Color(0xFFfab206);
  static const kGreenColor = Color(0xFF19b792);
  static const kWhiteColor = Color(0xFFFFFFFF);
  static const kBlueDarkColor = Color(0xFF1f3e76);
  static const kBlackColor = Color(0xFF000000);
  static const kGreyColor = Colors.grey;
}
