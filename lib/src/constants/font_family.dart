class FontFamily {
  FontFamily._();

  static const robotoRegular = 'Roboto';
  static const robotoCondensedRegular = 'RobotoCondensedBold';
  static const robotoCondensedBold = 'RobotoCondensedRegular';
}
