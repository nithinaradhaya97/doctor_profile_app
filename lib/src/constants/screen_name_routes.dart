class ScreenNamesRoutes {
  ScreenNamesRoutes._();

  static final PROVISION_PAGE = 'provision_page';
  static final VERIFYOTP_PAGE = 'verifyOtp_page';
  static final HOME_PAGE = 'home_page';
  static final DETAIL_PAGE = 'details_page';
  static final LOGIN_PAGE = 'login';
}
