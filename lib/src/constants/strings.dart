class Strings {
  Strings._();

  static final kAppNameString = "BHIMA DOCTOR";
  static final kEnterNumber = "ENTER YOUR MOBILE NUMBER";
  static final KSmsSubtitle =
      'We will send you an SMS with the verification code to this number';
  static final kContinueString = 'Continue';
  static final kEnterCode = 'ENTER VERIFICATION CODE';
  static final kverifyOtpSubtitle =
      'Please enter the verification code that was sent to ';
  static final kLoginString = 'Login';
  static final kTermsPolicy = 'I agree to the Terms Of Use and Privacy Policy';
  static final kAppBarName = 'BIMA DOCTOR';
  static final kEditProfile = 'EDIT PROFILE';
  static final kPersonalDetails = 'PERSONAL DETAILS';
  static final kFirstName = 'FIRST NAME';
  static final kLastname = 'LAST NAME';
  static final kGender = 'GENDER';
  static final kContactNumber = 'CONTACT NUMBER';
  static final kDob = 'DATE OF BIRTH';
  static final kDay = 'DAY';
  static final kMonth = 'MONTH';
  static final kYear = 'YEAR';
  static final kBloodGroup = 'BLOOD GROUP';
  static final kHeight = 'HEIGHT';
  static final kWeight = 'WEIGHT';
}
