import 'package:bhimadoctor/src/constants/screen_name_routes.dart';
import 'package:bhimadoctor/src/ui/widgets/detail_page.dart';
import 'package:bhimadoctor/src/ui/widgets/home_page.dart';
import 'package:bhimadoctor/src/ui/widgets/provision_page.dart';
import 'package:bhimadoctor/src/ui/widgets/verify_otp_page.dart';
import 'package:flutter/cupertino.dart';

class Routes {
  Routes._();
  static final routes = <String, WidgetBuilder>{
    ScreenNamesRoutes.PROVISION_PAGE: (BuildContext context) =>
        const ProvisionPage(),
    ScreenNamesRoutes.VERIFYOTP_PAGE: (BuildContext context) =>
        const VerifyOtpPage(),
    ScreenNamesRoutes.HOME_PAGE: (BuildContext context) => const HomePage(),
    ScreenNamesRoutes.DETAIL_PAGE: (BuildContext context) => const DetailPage(),
  };
}
