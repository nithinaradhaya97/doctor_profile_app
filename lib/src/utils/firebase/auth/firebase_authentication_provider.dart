import 'package:bhimadoctor/src/data/models/appUser.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/auth_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class FirebaseAuthenticationProvider implements AuthProvider, ChangeNotifier {
  late String mVerificationId;
  String mPhoneNumber = '';
  late AppUser appUser;
  late final FirebaseAuth auth;
// late VoidCallback _onVerification,_onVerificationFailed,_onCodeSent,_onCodeAutoRetrievalTimeout;
  String? fbIdToken;

  FirebaseAuthenticationProvider(this.auth) {
    print("#Init: Current User => ${auth.currentUser}");
    auth.idTokenChanges().listen((user) async {
      if (user != null) {
        fbIdToken = await user.getIdToken();
      } else {
        fbIdToken = null;
      }
      print("#Init: Current fbIdToken => $fbIdToken");
    });
  }
  var _authCredential;
  @override
  verifyAuthOtp(
      {required String smsCode,
      required Function(String) mCallBackSuccess}) async {
    _authCredential = PhoneAuthProvider.credential(
        verificationId: mVerificationId, smsCode: smsCode);
    if (_authCredential != null) {
      await auth
          .signInWithCredential(_authCredential)
          .then((result) => {
                if (result.user != null)
                  {
                    mCallBackSuccess.call("Success"),
                    appUser = AppUser(
                      uid: result.user!.uid,
                      displayName: result.user!.displayName!,
                      photoUrl: result.user!.photoURL!,
                      number: result.user!.phoneNumber!,
                    )
                  }
                else
                  {mCallBackSuccess.call("Failure")}
              })
          .catchError((onError) => {
                print('credential is invalid   ${onError}'),
                mCallBackSuccess.call("Failure")
              });
    }
  }

  @override
  verifyPhoneNumber(
      String number,
      Function(PhoneAuthCredential) onVerificationCompleted,
      Function(FirebaseAuthException) onVerificationFailed,
      Function(String, int) onCodeSent,
      Function(String) onCodeAutoRetrievalTimeout) async {
    await auth.verifyPhoneNumber(
        phoneNumber: number,
        verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {
          onVerificationCompleted.call(phoneAuthCredential);
        },
        verificationFailed: (FirebaseAuthException firebaseAuthException) {
          onVerificationFailed.call(firebaseAuthException);
        },
        codeSent: (String? verificationId, int? num) {
          onCodeSent.call(verificationId!, num!);
          mVerificationId = verificationId;
          mPhoneNumber = number;
          notifyListeners();
        },
        codeAutoRetrievalTimeout: (String str) {
          onCodeAutoRetrievalTimeout.call(str);
        });
  }

  @override
  Future<void> signOut() async {
    await auth.signOut();
  }

  @override
  Stream<User?> get authState => auth.idTokenChanges();

  @override
  String? getToken() {
    return fbIdToken;
  }

  @override
  void addListener(VoidCallback listener) {}

  @override
  void dispose() {}

  @override
  bool get hasListeners => throw UnimplementedError();

  @override
  void notifyListeners() {}

  @override
  void removeListener(VoidCallback listener) {}
}
