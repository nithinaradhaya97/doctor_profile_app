import 'package:bhimadoctor/src/data/models/appUser.dart';
import 'package:bhimadoctor/src/ui/widgets/verify_otp_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

@immutable
abstract class AuthProvider {
  Stream<User?> get authState;

  verifyPhoneNumber(
      String number,
      Function(PhoneAuthCredential) onVerificationCompleted,
      Function(FirebaseAuthException) onVerificationFailed,
      Function(String, int) onCodeSent,
      Function(String) onCodeAutoRetrievalTimeout);
  Future<void> signOut();

  verifyAuthOtp(
      {required String smsCode, required Function(void) mCallBackSuccess});

  // Future<AppUser> signIn(String email, String password);
  // Future<AppUser> signUp(String name, String email, String password);
  // bool isAuthenticated();

}
