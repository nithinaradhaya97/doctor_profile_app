// ignore: file_names
// ignore_for_file: file_names

import 'dart:convert';

import 'package:bhimadoctor/objectbox.g.dart';
import 'package:bhimadoctor/src/data/db/doctor_model_data_base.dart';
import 'package:bhimadoctor/src/data/models/doctor_model.dart';
import 'package:bhimadoctor/src/network/base_view_model.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:objectbox/src/native/store.dart';

class ClientApi extends BaseViewModel {
  List<DoctorModel> dList = [];
  late DBStoreHelper dbStoreHelper;
  final String baseUri = "https://5efdb0b9dd373900160b35e2.mockapi.io";
  late DoctorModel selectedItem;
  late int selectedIndex;
  getNetworkDoctorList(Store store) async {
    var url = '$baseUri/contacts';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      print('Successful Network call');
      var responseBody = response.body;
      var docMapList = json.decode(responseBody) as List;

      // delete from db
      // dbStoreHelper.deleteAllItems();

      late DoctorModel doctorModel;
      docMapList.forEach((json) => {
            doctorModel = DoctorModel.fromJson(json),
            dList.add(doctorModel),
            dList.sort((b, a) => a.rating.compareTo(b.rating)),
          });
      dbStoreHelper.insertAll(dList);

      // var entityDoctorList = await dbStoreHelper.queryAllPersons();
      // logger.d("Logger is working!  $entityDoctorList");
      // return await dbStoreHelper.queryAllPersons();

    } else {
      print('error Unsuccessful Network call');
    }
  }

  Future<List<dynamic>> getDoctorList(Store store) async {
    dbStoreHelper = DBStoreHelper(store);
    List<DoctorModel> list = await dbStoreHelper.queryAllPersons();
    if (list.isEmpty) {
      await getNetworkDoctorList(store);
    } else {
      dList = list;
    }
    return dList;
  }

  // Future docsFromDb() async {
  //   var list1 = await DoctorModelDataBase.instance.getAll();
  //   list = [];
  //   list.addAll(list1);
  //   return list1;
  // }

  update(DoctorModel doctorModel, int index) async {
    dbStoreHelper.editItem(doctorModel);
    dList.removeAt(index);
    dList.insert(index, doctorModel);
    notifyListeners();
  }
}
