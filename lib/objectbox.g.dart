// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: camel_case_types

import 'dart:typed_data';

import 'package:objectbox/flatbuffers/flat_buffers.dart' as fb;
import 'package:objectbox/internal.dart'; // generated code can access "internal" functionality
import 'package:objectbox/objectbox.dart';

import 'src/data/models/doctor_model.dart';

export 'package:objectbox/objectbox.dart'; // so that callers only have to import this file

final _entities = <ModelEntity>[
  ModelEntity(
      id: const IdUid(1, 970993328340777241),
      name: 'DoctorModel',
      lastPropertyId: const IdUid(19, 8707013505280163587),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 6458296442788944899),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 8643633968138230752),
            name: 'firstName',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 8443001410294952400),
            name: 'lastName',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 2816622889155904446),
            name: 'profilePic',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 2705788773669193417),
            name: 'favorite',
            type: 1,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 6698378312733411809),
            name: 'primaryContactNo',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 537082553561359827),
            name: 'rating',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 7385985914326062193),
            name: 'emailAddress',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(9, 5630705346506291427),
            name: 'qualification',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(10, 5878498432264851063),
            name: 'description',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(11, 8007506791921570799),
            name: 'specialization',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(12, 6702045745418858869),
            name: 'languagesKnown',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(13, 1686564389496250610),
            name: 'gender',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(14, 3824634751914867113),
            name: 'bloodGroup',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(15, 7165683295170887384),
            name: 'height',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(16, 1886337240990961798),
            name: 'weight',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(17, 1610701478769853314),
            name: 'day',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(18, 1640316114157629821),
            name: 'month',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(19, 8707013505280163587),
            name: 'year',
            type: 9,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[])
];

/// ObjectBox model definition, pass it to [Store] - Store(getObjectBoxModel())
ModelDefinition getObjectBoxModel() {
  final model = ModelInfo(
      entities: _entities,
      lastEntityId: const IdUid(1, 970993328340777241),
      lastIndexId: const IdUid(0, 0),
      lastRelationId: const IdUid(0, 0),
      lastSequenceId: const IdUid(0, 0),
      retiredEntityUids: const [],
      retiredIndexUids: const [],
      retiredPropertyUids: const [],
      retiredRelationUids: const [],
      modelVersion: 5,
      modelVersionParserMinimum: 5,
      version: 1);

  final bindings = <Type, EntityDefinition>{
    DoctorModel: EntityDefinition<DoctorModel>(
        model: _entities[0],
        toOneRelations: (DoctorModel object) => [],
        toManyRelations: (DoctorModel object) => {},
        getId: (DoctorModel object) => object.id,
        setId: (DoctorModel object, int id) {
          object.id = id;
        },
        objectToFB: (DoctorModel object, fb.Builder fbb) {
          final firstNameOffset = fbb.writeString(object.firstName);
          final lastNameOffset = fbb.writeString(object.lastName);
          final profilePicOffset = fbb.writeString(object.profilePic);
          final primaryContactNoOffset =
              fbb.writeString(object.primaryContactNo);
          final ratingOffset = fbb.writeString(object.rating);
          final emailAddressOffset = fbb.writeString(object.emailAddress);
          final qualificationOffset = fbb.writeString(object.qualification);
          final descriptionOffset = fbb.writeString(object.description);
          final specializationOffset = fbb.writeString(object.specialization);
          final languagesKnownOffset = fbb.writeString(object.languagesKnown);
          final genderOffset =
              object.gender == null ? null : fbb.writeString(object.gender!);
          final bloodGroupOffset = object.bloodGroup == null
              ? null
              : fbb.writeString(object.bloodGroup!);
          final heightOffset =
              object.height == null ? null : fbb.writeString(object.height!);
          final weightOffset =
              object.weight == null ? null : fbb.writeString(object.weight!);
          final dayOffset =
              object.day == null ? null : fbb.writeString(object.day!);
          final monthOffset =
              object.month == null ? null : fbb.writeString(object.month!);
          final yearOffset =
              object.year == null ? null : fbb.writeString(object.year!);
          fbb.startTable(20);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, firstNameOffset);
          fbb.addOffset(2, lastNameOffset);
          fbb.addOffset(3, profilePicOffset);
          fbb.addBool(4, object.favorite);
          fbb.addOffset(5, primaryContactNoOffset);
          fbb.addOffset(6, ratingOffset);
          fbb.addOffset(7, emailAddressOffset);
          fbb.addOffset(8, qualificationOffset);
          fbb.addOffset(9, descriptionOffset);
          fbb.addOffset(10, specializationOffset);
          fbb.addOffset(11, languagesKnownOffset);
          fbb.addOffset(12, genderOffset);
          fbb.addOffset(13, bloodGroupOffset);
          fbb.addOffset(14, heightOffset);
          fbb.addOffset(15, weightOffset);
          fbb.addOffset(16, dayOffset);
          fbb.addOffset(17, monthOffset);
          fbb.addOffset(18, yearOffset);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, Uint8List fbData) {
          final buffer = fb.BufferContext.fromBytes(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = DoctorModel(
              id: const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0),
              firstName:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 6, ''),
              lastName:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 8, ''),
              profilePic:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 10, ''),
              favorite: const fb.BoolReader()
                  .vTableGet(buffer, rootOffset, 12, false),
              primaryContactNo:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 14, ''),
              rating:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 16, ''),
              emailAddress:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 18, ''),
              qualification:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 20, ''),
              description:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 22, ''),
              specialization:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 24, ''),
              languagesKnown:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 26, ''),
              gender: const fb.StringReader()
                  .vTableGetNullable(buffer, rootOffset, 28),
              bloodGroup: const fb.StringReader()
                  .vTableGetNullable(buffer, rootOffset, 30),
              height: const fb.StringReader()
                  .vTableGetNullable(buffer, rootOffset, 32),
              weight: const fb.StringReader().vTableGetNullable(buffer, rootOffset, 34),
              day: const fb.StringReader().vTableGetNullable(buffer, rootOffset, 36),
              month: const fb.StringReader().vTableGetNullable(buffer, rootOffset, 38),
              year: const fb.StringReader().vTableGetNullable(buffer, rootOffset, 40));

          return object;
        })
  };

  return ModelDefinition(model, bindings);
}

/// [DoctorModel] entity fields to define ObjectBox queries.
class DoctorModel_ {
  static final id =
      QueryIntegerProperty<DoctorModel>(_entities[0].properties[0]);
  static final firstName =
      QueryStringProperty<DoctorModel>(_entities[0].properties[1]);
  static final lastName =
      QueryStringProperty<DoctorModel>(_entities[0].properties[2]);
  static final profilePic =
      QueryStringProperty<DoctorModel>(_entities[0].properties[3]);
  static final favorite =
      QueryBooleanProperty<DoctorModel>(_entities[0].properties[4]);
  static final primaryContactNo =
      QueryStringProperty<DoctorModel>(_entities[0].properties[5]);
  static final rating =
      QueryStringProperty<DoctorModel>(_entities[0].properties[6]);
  static final emailAddress =
      QueryStringProperty<DoctorModel>(_entities[0].properties[7]);
  static final qualification =
      QueryStringProperty<DoctorModel>(_entities[0].properties[8]);
  static final description =
      QueryStringProperty<DoctorModel>(_entities[0].properties[9]);
  static final specialization =
      QueryStringProperty<DoctorModel>(_entities[0].properties[10]);
  static final languagesKnown =
      QueryStringProperty<DoctorModel>(_entities[0].properties[11]);
  static final gender =
      QueryStringProperty<DoctorModel>(_entities[0].properties[12]);
  static final bloodGroup =
      QueryStringProperty<DoctorModel>(_entities[0].properties[13]);
  static final height =
      QueryStringProperty<DoctorModel>(_entities[0].properties[14]);
  static final weight =
      QueryStringProperty<DoctorModel>(_entities[0].properties[15]);
  static final day =
      QueryStringProperty<DoctorModel>(_entities[0].properties[16]);
  static final month =
      QueryStringProperty<DoctorModel>(_entities[0].properties[17]);
  static final year =
      QueryStringProperty<DoctorModel>(_entities[0].properties[18]);
}
