import 'package:bhimadoctor/src/constants/colors.dart';
import 'package:bhimadoctor/src/constants/screen_name_routes.dart';
import 'package:bhimadoctor/src/data/db/box_store.dart';
import 'package:bhimadoctor/src/data/models/doctor_model.dart';
import 'package:bhimadoctor/src/network/client_Api.dart';
import 'package:bhimadoctor/src/ui/reusable_widgets.dart';
import 'package:bhimadoctor/src/ui/widgets/provision_page.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/auth_provider.dart';
import 'package:bhimadoctor/src/utils/firebase/auth/firebase_authentication_provider.dart';
import 'package:bhimadoctor/src/utils/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'objectbox.g.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: CustomColors.kPrimaryDark,
    statusBarIconBrightness: Brightness.light, //top bar icons
  ));

  runApp(MyApp());
}

enum LoadStatus { LOADING, FBLOADING_ERROR, LOADED }

class MyApp extends StatelessWidget {
  late FirebaseAuthenticationProvider authProvider;
  late Store store;
  Future<LoadStatus> init() async {
    FirebaseApp fbApp = await Firebase.initializeApp();

    FirebaseAuth fbAuth = FirebaseAuth.instanceFor(app: fbApp);
    User? fbUser = fbAuth.currentUser;
    authProvider = new FirebaseAuthenticationProvider(fbAuth);
    print("#init:  App done");
    store = await BoxStore.getStore();

    return LoadStatus.LOADING;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: init(),
      builder: (context, snapshot) {
        print("#init: App init state ${snapshot} ");
        // Check for errors
        if (snapshot.hasError) {
          return ErrorMessage("${snapshot}");
        }
        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(providers: [
            ChangeNotifierProvider<FirebaseAuthenticationProvider>(
                create: (_) => authProvider),
            StreamProvider(
              create: (context) => context.read<AuthProvider>().authState,
              initialData: null,
            ),
            ChangeNotifierProvider<ClientApi>(create: (_) => ClientApi()),
            Provider(create: (_) => store)
          ], child: AutoRoutePage());
        }
        return Loading();
      },
    );
  }
}

class AutoRoutePage extends StatelessWidget {
  // const AutoRoutePage(FirebaseAuthenticationProvider authProvider, {Key? key})
  //     : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: Routes.routes,
      // home: ProvisionPage(),
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.red,
          primaryColorDark: CustomColors.kPrimaryDark),
      initialRoute: ScreenNamesRoutes.PROVISION_PAGE,
    );
  }
}
